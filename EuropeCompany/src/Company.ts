import { Employee } from './Employee';
export class Company {
    private employees: Employee[] = [];

    add(employee: Employee): void {
        this.employees.push(employee);
    }

    getProjectList(): string[] {
        const projects = new Set<string>();
        for (let i = 0; i < this.employees.length; i++) {
            projects.add(this.employees[i].getCurrentProject());
        }
        return Array.from(projects);
    }

    getNameList(): string[] {
        return this.employees.map(employee => employee.getName());
    }
}
import { Company } from './Company';
import { Frontend } from './Frontend';
import { Backend } from './Backend';

console.log("EuropeCompany");

const myCompany = new Company();
const frontendEmployee = new Frontend('John', 'Project 1');
const backendEmployee = new Backend('Jane', 'Project 2');

myCompany.add(frontendEmployee);
myCompany.add(backendEmployee);

console.log("ProjectList:");
console.log(myCompany.getProjectList());

console.log("NameList:");
console.log(myCompany.getNameList());
import {ILocation} from './ILocation';
import {Employee} from "./Employee";

export class CompanyLocationLocalStorage implements ILocation {
    private readonly key: string;

    constructor(key: string = 'employees') {
        this.key = key;
        localStorage.setItem(this.key, JSON.stringify([]));
    }

    addPerson(employee: Employee): void {
        let employees = this.getEmployees();
        employees.push(employee);
        let stringEmployees = JSON.stringify(employees);
        localStorage.setItem(this.key, stringEmployees);
    }

    getCount(): number {
        const employees = this.getEmployees();
        return employees.length;
    }

    getPerson(index: number): Employee {
        const employees = this.getEmployees();
        if (index < 0 || index >= employees.length) {
            throw new Error('Index out of bounds');
        }
        return employees[index];
    }

    private getEmployees(): Employee[] {
        let stringEmployees = localStorage.getItem(this.key) || '[]';
        let raw = JSON.parse(stringEmployees);
        return raw.map((obj: { name: string, currentProject: string }) => Employee.fromObject(obj));
    }
}
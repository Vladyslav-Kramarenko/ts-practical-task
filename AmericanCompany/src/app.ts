import {Company} from './Company';
import {Frontend} from './Frontend';
import {Backend} from './Backend';

console.log("AmericanCompany");

const myCompany = new Company();
const frontendEmployee1 = new Frontend('John', 'Project 1');
const backendEmployee1 = new Backend('Jane', 'Project 2');
const frontendEmployee2 = new Frontend('Bill', 'Project 1');
const backendEmployee2 = new Backend('Jack', 'Project 2');


myCompany.add(frontendEmployee1);
myCompany.add(backendEmployee1);
myCompany.add(frontendEmployee2);
myCompany.add(backendEmployee2);

console.log("ProjectList:");
console.log(myCompany.getProjectList());
console.log("NameList:");
console.log(myCompany.getNameList());
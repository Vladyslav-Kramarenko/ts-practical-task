import {IEmployee} from './IEmployee';

export class Backend implements IEmployee {
    private currentProject: string;
    private name: string;

    constructor(name: string, currentProject: string) {
        this.name = name;
        this.currentProject = currentProject;
    }

    getCurrentProject(): string {
        return this.currentProject;
    }

    getName(): string {
        return this.name;
    }
}
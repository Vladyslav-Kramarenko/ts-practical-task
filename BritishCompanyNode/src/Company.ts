import {Employee} from './Employee';
import {ILocation} from './ILocation';

export class Company<T extends ILocation> {
    private location: T;

    constructor(location: T) {
        this.location = location;
    }

    add(employee: Employee): void {
        this.location.addPerson(employee);
    }

    getProjectList(): string[] {
        const projects = new Set<string>();
        const count = this.location.getCount();
        for (let i = 0; i < count; i++) {
            const employee = this.location.getPerson(i);
            projects.add(employee.getCurrentProject());
        }
        return Array.from(projects);
    }

    getNameList(): string[] {
        const names: string[] = [];
        const count = this.location.getCount();
        for (let i = 0; i < count; i++) {
            const employee = this.location.getPerson(i);
            names.push(employee.getName());
        }
        return names;
    }
}
import {CompanyLocationLocalStorage} from "./CompanyLocationLocalStorage";
import {CompanyLocationArray} from "./CompanyLocationArray";
import {Employee} from "./Employee";
import {Company} from "./Company";

const employee1 = new Employee('John', 'Project 1');
const employee2 = new Employee('Jane', 'Project 2');
const employee3 = new Employee('Jack', 'Project 1');
const employee4 = new Employee('Bill', 'Project 2');

const localStorageCompany1 = new Company(new CompanyLocationLocalStorage('local_company_1'));
const localStorageCompany2 = new Company(new CompanyLocationLocalStorage('local_company_2'));

localStorageCompany1.add(employee1);
localStorageCompany1.add(employee2);

localStorageCompany2.add(employee3);
localStorageCompany2.add(employee4);

console.log("localStorageCompany1:");
console.log("- ProjectList:");
console.log(localStorageCompany1.getProjectList());
console.log("- NameList:");
console.log(localStorageCompany1.getNameList());

console.log("localStorageCompany2:");
console.log("- ProjectList:");
console.log(localStorageCompany2.getProjectList());
console.log("- NameList:");
console.log(localStorageCompany2.getNameList());

const arrayCompany1 = new Company(new CompanyLocationArray());
const arrayCompany2 = new Company(new CompanyLocationArray());

arrayCompany1.add(employee1);
arrayCompany1.add(employee2);
arrayCompany1.add(employee3);

arrayCompany2.add(employee2);
arrayCompany2.add(employee3);
arrayCompany2.add(employee4);

console.log("arrayCompany1:");
console.log("- ProjectList:");
console.log(arrayCompany1.getProjectList());
console.log("- NameList:");
console.log(arrayCompany1.getNameList());
console.log("arrayCompany2:");
console.log("- ProjectList:");
console.log(arrayCompany2.getProjectList());
console.log("- NameList:");
console.log(arrayCompany2.getNameList());